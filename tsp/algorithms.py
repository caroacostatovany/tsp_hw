"""
This module contains multiple algorithms such as:
- Greedy TSP
- Ant Colony

última modificación: Carolina Acosta, 24/nov/2020
"""

import copy
import random
import numpy as np

from tsp import Tour


class GreedyTSP():
    """
    Implements Greedy TSP algorithm
    """
    def __init__(self, **hyperparams):
        self.initial = hyperparams.get('initial')

    def run(self, tsp):
        """
        Selecciona los siguientes nodos a visitar de manera greedy
        """

        if self.initial is None:
            self.initial = np.random.choice(tsp.places)

        current = self.initial

        tour = Tour()
        tour.append(current)

        while not tour.closed:

            distances_to = {next_place: tsp.cost(current, next_place)
                            for next_place in
                            tsp.missing(list(tour.visited_places()))}

            current = min(distances_to, key=distances_to.get)
            tour.append(current)

            if len(tour) == len(tsp.places):
                tour.close()

        cost = tsp.total_cost(tour)

        return {'cost': cost, 'tour': tour}

    def __str__(self):
        return f"{self.__class__} [initial: {self.initial}]"


class Ant:
    """
    Representa a un agente "hormiga" del algoritmo ACO
    """

    def __init__(self, colony, initial_state):
        self.current = initial_state
        # self.next_city = None
        self.tour = Tour()
        self.colony = colony

    def smell(self, to_city):
        """
        This function will tell you the intensity of smell
        """
        current = self.current.place_id
        to_city = to_city.place_id

        return \
            self.colony.pheromones[current][to_city]**self.colony.alpha *\
            self.colony.eta[current][to_city]**self.colony.beta

    @property
    def not_visited(self):
        """
        The places the colony has not visited
        """
        return set(self.colony.places).difference(set(self.tour))

    @property
    def tour_finished(self):
        """
        If the tour for the ant has finished
        """
        return not self.not_visited

    def move(self):
        """
        The movement of the ant
        """
        odor = 0.0

        for to_city in self.not_visited:
            if to_city != self.current:
                odor += self.smell(to_city)

        random.seed(1993)
        threshold = np.random.rand()
        for to_city in self.not_visited:
            prob_current_next = self.smell(to_city)/odor
            threshold -= prob_current_next
            if threshold <= 0:
                self.tour.append(to_city)

    def travel(self):
        """
        Keep moving if the tour has not finished yet
        """
        while not self.tour_finished:
            self.move()

    def __str__(self):
        """
        Convert initial hyperparams to string
        """
        return f"{self.__class__} [current: {self.current}," \
               f"Tour: {self.tour}, colony: {self.colony}]"


class AntColony():
    """
    Implementa el algoritmo ACO básico
    """

    # pylint: disable=too-many-instance-attributes
    def __init__(self, **hyperparams):
        # Init hyperparameters
        self.alpha = hyperparams.get('alpha', 1)
        self.beta = hyperparams.get('beta', 1)
        self.rho = hyperparams.get('rho', 0.5)
        self.q_matrix = hyperparams.get('Q', 100)
        self.max_steps = hyperparams.get('max_steps', 100)

        # Init related with the ant
        self.best = np.inf
        self.best_path = []
        self.steps = 0
        self.ants = []
        self.initial_pheromone = 0.0
        self.pheromones = np.full([0, 0], fill_value=self.initial_pheromone)
        self.eta = []
        self.tsp = None

    def init(self, tsp):
        """
        Initialize the Ant Colony
        """
        self.tsp = tsp

        self.ants = [Ant(self, place) for place in self.tsp.places]
        self.initial_pheromone = 1.0/len(self.tsp.places)
        self.pheromones = np.full([len(self.tsp.places), len(
            self.tsp.places)], fill_value=self.initial_pheromone)

        self.eta = list(1.0/np.array(self.tsp.distances))

    def reset(self):
        """
        Reinitialize the colony
        """
        self.init(self.tsp)

    @property
    def places(self):
        """
        Places from tsp
        """
        return self.tsp.places

    @property
    def done(self):
        """
        If the max steps have been reached
        """
        return self.steps > self.max_steps

    def update_pheromones(self):
        """
        Update the pheromones on the colony
        """
        # self.pheromones *= (1.0 - self.rho)
        self.pheromones *= (1.0 - self.rho)
        self.pheromones[self.pheromones < 0.0] = self.initial_pheromone

        for ant in self.ants:
            for (current, to_city) in ant.tour:
                self.pheromones[current.place_id][to_city.place_id] += \
                    self.q_matrix/len(ant.tour)
                self.pheromones[to_city.place_id][current.place_id] = \
                    self.pheromones[current.place_id][to_city.place_id]

        self.pheromones *= self.rho

    def update_best(self):
        """
        Calculate the best tour according with the distance
        """

        for ant in self.ants:
            if len(ant.tour) < self.best:
                self.best = len(ant.tour)
                self.best_path = copy.copy(ant.tour)

    def step(self):
        """
        Move all ants on the colony
        """
        for ant in self.ants:
            ant.move()

        self.update_best()
        self.update_pheromones()
        self.steps += 1

    def run(self, tsp):
        """
        Run the algorithm
        """
        # self.tsp = tsp
        self.init(tsp)

        while not self.done:
            self.step()

        cost = tsp.total_cost(self.best_path)
        return {'cost': cost, 'tour': self.best_path}

    def __str__(self):
        """
        Convert initial hyperparams to string
        """
        return f"{self.__class__} [alpha: {self.alpha}," \
               f"beta: {self.beta}, rho: {self.rho}," \
               f"Q: {self.q_matrix}, max_steps: {self.max_steps}]"
