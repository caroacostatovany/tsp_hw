"""
Solver module
"""

from dataclasses import dataclass


@dataclass()
class Solver:
    """
    Solver class that will tell the solutions for the algorithms given
    """

    def __init__(self, algorithms):
        self.algorithms = algorithms
        self.solutions = {}

    def solve(self, tsp):
        """
        Solve with multiple algorithms
        """
        print(f"Usaremos {len(self.algorithms)} "
              f"algoritmos para resolver este problema.")
        for algorithm in self.algorithms:
            print(f"Solving TSP problem with {algorithm}")
            self.solutions[str(algorithm)] = algorithm.run(tsp)
            print(self.solutions[str(algorithm)]['cost'])

        return self.solutions
