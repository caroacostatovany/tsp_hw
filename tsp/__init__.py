"""
Módulo para clases TSP, Place, Tour
última modificación: Carolina Acosta, 24/nov/2020
"""

from dataclasses import dataclass
import numpy as np
from scipy.spatial import distance
# from typing import List
from more_itertools import pairwise
import matplotlib.pyplot as plt

MAX_DISTANCE = 100


class TSP:
    """
    Describes a TSP
    """
    def __init__(self, coordinates, distances):
        self.places = [Place(id, v[0], v[1])
                       for id, v in enumerate(coordinates)]
        self.distances = [d for id, d in enumerate(distances)]

    def cost(self, place, another_place):
        """
        Returns the distance(cost) between places
        """
        return self.distances[place.place_id][another_place.place_id]

    def total_cost(self, tour):
        """
        Calculates the total cost of the tour
        (This shouldn't be on the tour class?)
        """
        total_cost = 0.0
        for current, to_city in tour:
            total_cost += self.cost(current, to_city)

        return total_cost

    def missing(self, tour_places):
        """
        This function indicates missing places to visit
        COMENTARIO: En la tarea explicaba que fuera if statement
        En vez de los lugares.
        """
        return set(self.places) - set(tour_places)
        # missing_places = False
        # for current in self.places:
        #    if current not in tour.visited_places:
        #        missing_places = True
        # return missing_places

    @classmethod
    def from_files(cls, coordinates_file, distances_file=None):
        """
        Initialize TSP coordinates and distance matrix from files
        """
        coordinates = np.loadtxt(coordinates_file)
        if distances_file is None:
            distances = distance.cdist(coordinates,
                                       coordinates,
                                       'euclidean')
        else:
            distances = np.loadtxt(distances_file)
        return TSP(coordinates, distances)

    @classmethod
    def from_random(cls, num_places=50, max_distance=MAX_DISTANCE):
        """
        Initialize TSP coordinates from random
        """
        coordinates = np.random.randint(low=0,
                                        high=max_distance,
                                        size=(num_places, 2))

        distances = distance.cdist(coordinates,
                                   coordinates,
                                   'euclidean')
        return TSP(coordinates, distances)

    def plot_cities(self):
        """
        This function will plot the complete places on the TSP
        """
        # plt.scatter(cities[:,0], cities[:,1])
        plt.plot(self.places[:, 0], self.places[:, 1], 'co')
        plt.xlim(0, MAX_DISTANCE)
        plt.ylim(0, MAX_DISTANCE)
        plt.show()


@dataclass(eq=True, frozen=True)
class Place:
    """A place to be visited"""
    place_id: int
    x_coordinate: float
    y_coordinate: float


class Tour:
    """
    A Tour defined
    """
    def __init__(self):
        self._path = []

    @property
    def initial(self):
        """
        Returns the first place visited
        """
        return self._path[0]

    @property
    def current(self):
        """
        Returns the last place on the list
        """
        return self._path[-1]

    @property
    def closed(self):
        """
        This function will tell you if the tour is closed or not
        """
        closed = False
        if len(self._path) > 1:
            closed = self._path[0] == self._path[-1]

        return closed

    def close(self):
        """
        This function will close the tour
        """
        if len(self._path) > 1:
            self._path.append(self._path[0])

    def missing(self, places):
        """
        This function will tell what places are missing
        """
        return set(places) - set(self._path)

    def append(self, place):
        """
        This function will add a place visited to the tour class
        """
        self._path.append(place)

    def visited_places(self):
        """
        This function will tell you what are the visited places
        """
        return self._path

    def plot_solution(self, places):
        """
        This function will plot the solution according to the places given
        and the path the tour has done.
        """
        plt.plot(places[:, 0], places[:, 1], 'co')
        plt.xlim(0, MAX_DISTANCE)
        plt.ylim(0, MAX_DISTANCE)

        for (_from, _to) in pairwise(self._path):
            plt.arrow(places[_from][0], places[_from][1],
                      places[_to][0] - places[_from][0],
                      places[_to][1] - places[_from][1],
                      color='b', length_includes_head=True)

        # Close the loop
        last = self._path[-1]
        first = self._path[0]
        plt.arrow(places[last][0], places[last][1],
                  places[first][0] - places[last][0],
                  places[first][1] - places[last][1],
                  color='b', length_includes_head=True)

        plt.show()

    def __len__(self):
        return len(self._path)

    def __iter__(self):
        return ((current, other) for (current, other) in pairwise(self._path))

    def __repr__(self):
        return f"Tour: [{'->'.join([str(place) for place in self._path])}]"

    def __str__(self):
        return f"Tour: " \
               f"[{'->'.join([str(place.place_id) for place in self._path])}]"


__version__ = '0.1.0'

# from .cli import cli
