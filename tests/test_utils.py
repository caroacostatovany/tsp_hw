import pytest

from config import settings
from itertools import product

from tsp import Place
from tsp.algorithms import GreedyTSP, AntColony

from tsp.utils import process_grid, load_algorithm, flatten_algorithm_grid

def test_process_grid():
    greedy = GreedyTSP()
    aoc = AntColony()

    grids = [{'algorithm': 'tsp.algorithms.GreedyTSP'},
             {'algorithm': 'tsp.algorithms.AntColony'}]

    algorithms = process_grid(grids)
    algorithms_type = [type(item) for item in algorithms]

    assert algorithms_type == [type(greedy),type(aoc)]

def test_load_algorithm():
    greedy = GreedyTSP()
    classpath = "tsp.algorithms.GreedyTSP"

    instance = load_algorithm(classpath)
    assert instance.__class__ == greedy.__class__

def test_flatten_algorithm_grid():
    grid = {'algorithm': 'tsp.algorithms.AntColony',
              'hyperparameters': {'alpha': [0.2],
                                  'beta': [1.0]}}

    hp_grid = grid['hyperparameters']
    dictionary_expected = {'alpha': 0.2, 'beta': 1.0}
    param = list(flatten_algorithm_grid(hp_grid))
    assert param[0] == dictionary_expected

    # Empty parameters
    grid = {'algorithm': 'tsp.algorithms.GreedyTSP',
              'hyperparameters': {}}
    hp_grid = grid['hyperparameters']
    param = list(flatten_algorithm_grid(hp_grid))
    assert param[0] == {}