
import pytest
from pytest import approx

import numpy as np

from tsp import Place
from tsp.algorithms import AntColony, GreedyTSP, Ant


def test_greedy(tsp_obj):

    g = GreedyTSP(initial=Place(3, 1, 1))

    solution = g.run(tsp_obj)

    assert int(solution['cost']) == 21


def test_aoc(tsp_obj):
    colony = AntColony()

    solution = colony.run(tsp_obj)
    print(solution['cost'])
    # assert approx(int(solution['cost'])) == 1990
    true_statement = 1750 <= int(solution['cost']) <= 2050
    assert true_statement


def test_ant(tsp_obj):
    colony = AntColony()
    place = Place(3,1,1)

    ant = Ant(colony, place)
    assert ant.current == place


def test_ant_smell(tsp_obj):
    colony = AntColony()
    colony.initial_pheromone = 1.0/len(tsp_obj.places)
    colony.pheromones = np.full([len(tsp_obj.places), len(
            tsp_obj.places)], fill_value=colony.initial_pheromone)
    colony.eta = list(1.0/np.array(tsp_obj.distances))

    # colony.init(tsp_obj)
    ant = Ant(colony, tsp_obj.places[0])
    # ant.smell()
    # Omitiré esta prueba por lo mientras porque smell depende de
    # la colonia, feromonas y la matriz eta
    pass

