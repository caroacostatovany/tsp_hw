# TSP

## Datos

Los archivos contenidos en la carpeta `data` provienen originalmente
de [National Traveling Salesman Problems](https://www.math.uwaterloo.ca/tsp/world/countries.html).

Pero el formato fué modificado para este proyecto.

| Original                | Nombre               |
|:-----------------------:|:--------------------:|
| ar9162.tsp              | argentina.tsp        |
| att_48.tsp, att48_d.txt | eua.tsp, eua48_d.tsp |
| dantzig42_d.tsp         | dantzig_d.tsp        |
| nu3496.tsp              | nicaragua.tsp        |
| qa194.tsp               | qatar.tsp            |
| ym7663.tsp              | yemen.tsp            |
| p01_d.txt, p01_xy.txt   | p01.tsp, p01_d.tsp   |

Los archivos sin sufijo `_d` contienen las coordenadas. Los que tienen
el sufijo `_d` la matriz de distancia.


Para el ejercicio `calculate_tour --random solve` con 5 algoritmos:

 | algoritmo | hiperparámetros | costo |
 |-----------|-----------------|-------|
 | GreedyTSP | Ninguno | 736.8801364243461 |
 | AntColony | `[alpha: 0.5,beta: 1.0, rho: 0.5,Q: 10, max_steps: 100]` | 0.0 |
 | AntColony | `[alpha: 0.5,beta: 2.0, rho: 0.5,Q: 10, max_steps: 100]` | 0.0 |
 | AntColony | `[alpha: 1.0,beta: 1.0, rho: 0.5,Q: 10, max_steps: 100]` | 0.0 |
 | AntColony | `[alpha: 1.0,beta: 2.0, rho: 0.5,Q: 10, max_steps: 100]` | 0.0 |
 
 Pareciera que algo raro sucede en el algoritmo AntColony 


Para el ejercicio `calculate_tour --coordinates_file ./data/argentina.tsp solve`:
Sólo utilizando el algoritmo `GreedyTSP` sin ningún lugar inicial, estos son los resultados:

| archivo | comando para ejecutar calculate_tour | costo | 
| ------- | -----------------------|-------|
| argentina.tsp | `calculate_tour --coordinates_file ./data/argentina.tsp solve`| 1065983.1330800692 |
| att48.tsp | `calculate_tour --coordinates_file ./data/att48.tsp  --distances_file ./data/att48_d.tsp solve` |  41955.0 |
| nicaragua.tsp | `calculate_tour --coordinates_file ./data/nicaragua.tsp solve`| 121326.98429449572 |
| p01.tsp | `calculate_tour --coordinates_file ./data/p01.tsp  --distances_file ./data/p01_d.tsp solve` |  299.0 |
| qatar.tsp | `calculate_tour --coordinates_file ./data/qatar.tsp solve`| 2332.627148276142 |
| yemen.tsp | `calculate_tour --coordinates_file ./data/yemen.tsp solve`| 305125.3789473947 |

Modificando un poco el `tsp.toml` para no generar 25 algoritmos, sino sólo 5 algoritmos:
Para `p01.tsp` con sus respectivas distancias: (omitiré el tour ya que puede ser largo)

 | algoritmo | hiperparámetros | costo |
 |-----------|-----------------|-------|
 | GreedyTSP | Ninguno | 317.0 |
 | AntColony | `[alpha: 0.5,beta: 1.0, rho: 0.5,Q: 10, max_steps: 100]` | 8152.0 |
 | AntColony | `[alpha: 0.5,beta: 2.0, rho: 0.5,Q: 10, max_steps: 100]` | 3699.0 |
 | AntColony | `[alpha: 1.0,beta: 1.0, rho: 0.5,Q: 10, max_steps: 100]` | 23842.0 |
 | AntColony | `[alpha: 1.0,beta: 2.0, rho: 0.5,Q: 10, max_steps: 100]` | 2377.0 | 